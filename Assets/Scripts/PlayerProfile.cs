﻿#region

using UnityEngine;

#endregion

public class PlayerProfile : MonoBehaviour {

    private void Awake() {
        CheckFirstRun();
    }

    private static float _devCoins;
    public static float DevCoints {
        get { return _devCoins; }
        set {
            _devCoins = value;
            PlayerPrefs.SetFloat("tycoonDevCoins",value);
        }
    }

    private void CheckFirstRun() { // TODO: Fix first time launch ( in the end since im developing rn)
        if (PlayerPrefs.HasKey("tycoonLaunchedFirstTime")) {
   
        }
        else {

            DevCoints = 100;
        }
      
    }
}