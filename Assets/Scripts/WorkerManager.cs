﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;


public class WorkerManager : MonoBehaviour {

    public GameObject WorkerPrefab;
    [SerializeField] public UIController _uiController;
    private static WorkerManager instance = null;
    public int nextID;
    public int testInt = 2;

    private void Start() {
       // UIController.GetInstance().RefreshBackgroundUI();
    }

    public static WorkerManager GetInstance() {
        if (instance == null) {
            instance = new WorkerManager();
        }
        return instance;
    }

    public static List<Worker> WorkersList(Worker.Worker_Type workType) {

        List<Worker> Programmers = new List<Worker>();
        List<Worker> Artists = new List<Worker>();
        List<Worker> SoundArtists = new List<Worker>();
        List<Worker> BusinessPeople = new List<Worker>();

        foreach (var newWorker in Worker.Workers) {
            if (newWorker.WorkerType == Worker.Worker_Type.Programmer) {
                Programmers.Add(newWorker);
            }
            if (newWorker.WorkerType == Worker.Worker_Type.Artist) {
                Artists.Add(newWorker);
            }
            if (newWorker.WorkerType == Worker.Worker_Type.Sound) {
                SoundArtists.Add(newWorker);
            }
            else {
                BusinessPeople.Add(newWorker);
            }
        }

        if (workType == Worker.Worker_Type.Programmer) {
            return Programmers;
        }
        else if (workType == Worker.Worker_Type.Artist) {
            return Artists;
        }
        else if (workType == Worker.Worker_Type.Sound) {
            return SoundArtists;
        }
        else {
            return BusinessPeople;
        }
    }

   


}



[CustomEditor(typeof(WorkerManager))]

public class WorkerManagerEditor : Editor {



    public override void OnInspectorGUI() {

        DrawDefaultInspector();
        WorkerManager wManager = (WorkerManager) target; // Passer

        if (GUILayout.Button("Create testing worker.")) {
            WorkerManager.GetInstance().nextID++;
            GameObject newWorker = (GameObject) Instantiate(wManager.WorkerPrefab);
            wManager._uiController.RefreshBackgroundUI();
        }

    }

}