﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Remoting.Messaging;
using LitJson;

public class JSONContent : MonoBehaviour {


    private static JSONContent instance;

    public JsonData playerProfileData;
    public string playerProfilePath;
    public int testInt = 5;


    public JsonData GetItem(string name) {
        return playerProfileData[name];

    }

    void Awake() {
        playerProfilePath = File.ReadAllText(Application.dataPath + "/Resources/PlayerProfile.json");
        playerProfileData = JsonMapper.ToObject(playerProfilePath);
        instance = this;
        //   Debug.Log(GetItem("DevCoins"));

    }


    public static JSONContent Instance{
        get {
            if (instance == null) {
                instance = new JSONContent();
            }
            return instance;
        }
       
    }


    public class PlayerProfile {
        public int DevCoins { get; set; }
        public string PlayerName { get; set; }

        public PlayerProfile(int DevCoins, string PlayerName) {
            this.DevCoins = DevCoins;
            this.PlayerName = PlayerName;
        }
    }

}

