﻿using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour
{

    [SerializeField]private string _gameName;
    [SerializeField] private int _gameGraphics;
    [SerializeField] private int _gameOptimization;
    [SerializeField] private int _gameSounds;

    public string GameName
    {
        get { return _gameName; }
        set { _gameName = value; }
    }


    public int GameGraphics
    {
        get { return _gameGraphics; }
        set { _gameGraphics = value; }
    }

    public int GameOptimization
    {
        get { return _gameOptimization; }
        set { _gameOptimization = value; }
    }

    public int GameSounds
    {
        get { return _gameSounds; }
        set { _gameSounds = value; }
    }

    [HideInInspector]public GameObject _GameObject;

    public Game(string gName,int gGraphics,int gOptimiz,int gSounds, GameObject gamePrefab) {
        _GameObject = GameObject.Instantiate(gamePrefab);
        Init(gName,gGraphics,gOptimiz,gSounds);


    }

    public void Init(string gName, int gGraphics, int gOptimiz, int gSounds)
    {
        _GameObject.name = gName;
        _GameObject.GetComponent<Game>().GameName = gName;
        _GameObject.GetComponent<Game>().GameGraphics = gGraphics;
        _GameObject.GetComponent<Game>().GameOptimization = gOptimiz;
        _GameObject.GetComponent<Game>().GameSounds = gSounds;
    }

}


