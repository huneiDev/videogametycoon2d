﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

    private static GameManager instance;
    public GameObject CreationMenu;
    [SerializeField] private GameObject _bgCanvas;
    public List<Game> CurrentGames = new List<Game>();

    public static GameManager GetInstance() {
        if (instance == null) {
            instance = new GameManager();
        }
        return instance;
    }

    //TODO :: Save games.
    public void BeginCreateCanvas()
    {
        GameObject.Instantiate(CreationMenu);
        _bgCanvas.SetActive(false);
    }
   


}
