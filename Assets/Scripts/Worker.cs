﻿#region

using System.Collections.Generic;
using UnityEngine;

#endregion

public class Worker : MonoBehaviour
{
    [HideInInspector]
    public enum Worker_Type
    {
        Artist,
        Programmer,
        Business,
        Sound
    }

    [SerializeField] private int _workerAge;
    //--------------------------------------------------
    [SerializeField] private int _workerID;
    [SerializeField] private int _workerLevel;
    [SerializeField] private string _workerName;
    [SerializeField] private Worker_Type _workerType;
    [SerializeField] private float _workerValue;
    public GameObject WorkerGameObject;
    //--------------------------------------------------
    public static List<Worker> Workers = new List<Worker>();  

    public Worker_Type WorkerType
    {
        get { return _workerType; }
        set { _workerType = value; }
    }

    public int WorkerID
    {
        get { return _workerID; }
        set { _workerID = value; }
    }

    public int WorkerLevel
    {
        get { return _workerLevel; }
        set { _workerLevel = value; }
    }

    public float WorkerValue
    {
        get { return _workerValue; }
        set { _workerValue = value; }
    }

    public int WorkerAge
    {
        get { return _workerAge; }
        set { _workerAge = value; }
    }

    public string WorkerName
    {
        get { return _workerName; }
        set { _workerName = value; }
    }

    //--------------------------------------------------
    public static int WorkerCount { get; set; }

    private void Start()
    {
        Init();
    }

    public void Init(Worker_Type wType, int wLevel, float wValue, int wAge, string wName)
    {
        //TODO:: Add Random names from a file.
        WorkerID = WorkerManager.GetInstance().nextID;
        WorkerType = wType;
        WorkerLevel = wLevel;
        WorkerAge = wAge;
        WorkerName = wName;
        Workers.Add(this);
        if (WorkerCount == 0)
        {
            WorkerCount = 2;
        }
        else
        {
            WorkerCount++;
        }
    }

    public void Init()
    {
        WorkerID = WorkerManager.GetInstance().nextID;
        WorkerType = Worker_Type.Programmer;
        WorkerLevel = 1;
        WorkerAge = 18;
        WorkerName = "Generic Don";
        gameObject.name = WorkerName + WorkerID;
        Workers.Add(this);
        if (WorkerCount == 0)
        {
            WorkerCount = 1;
        }
        else
        {
            WorkerCount++;
        }
    }
}