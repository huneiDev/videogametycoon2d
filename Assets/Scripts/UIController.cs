﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    [SerializeField]
    private Text DevCoinsText;
    [SerializeField]
    private Text WorkersText;

    [SerializeField] private GameObject createGameButton;
    [SerializeField] private GameObject shopButton;
    [SerializeField] private GameObject gameCreationCanvas;

    private static UIController instance;
    [HideInInspector]public GameObject CurrentCreationCanvas;

    public static UIController GetInstance() {
        if (instance == null) {
            instance = new UIController();
        }
        return instance;

    }

    void Start() {
        RefreshBackgroundUI();
    }

    public void ShowShopMenu(GameObject shopCanvas) {
        createGameButton.SetActive(false);
        shopButton.SetActive(false);
        CurrentCreationCanvas = GameObject.Instantiate(gameCreationCanvas);
    }

    public void RefreshBackgroundUI() {
        DevCoinsText.text = "Dev Coins: " + JSONContent.Instance.GetItem("DevCoins");
        WorkersText.text = "Workers: " + WorkerManager.GetInstance().nextID;
    }


}
