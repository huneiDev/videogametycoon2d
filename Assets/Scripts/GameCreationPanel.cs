﻿using System.Collections;
using System.Security.Policy;
using UnityEngine;
using UnityEngine.UI;

public class GameCreationPanel : MonoBehaviour
{

    [SerializeField] private string _gameName;
    [SerializeField] private GameObject _gamePrefab;
    [SerializeField] private int _gameGraphics;
    [SerializeField] private int _gameOptimization;
    [SerializeField] private int _gameSounds;
    [SerializeField] private Slider _graphicsSlider;
    [SerializeField] private Slider _soundsSlider;
    [SerializeField] private Dropdown _graphicsDropDown;
    [SerializeField] private Dropdown _soundsDropDown;
    [SerializeField] private Dropdown _optimizationDropDown;
    [SerializeField] private Slider _optimizationSlider;
    [SerializeField] private InputField _gameNameField;
    [SerializeField] private Text _graphicsText;
    [SerializeField] private Text _soundText;
    [SerializeField] private Text _optimizationText;


    private void Start() {
        for (int i = 1; i < WorkerManager.WorkersList(Worker.Worker_Type.Programmer).Count; i++) {
            _optimizationDropDown.options.Add(new Dropdown.OptionData(i.ToString()));
        }
        for (int b = 1; b < WorkerManager.WorkersList(Worker.Worker_Type.Artist).Count; b++) {
            _graphicsDropDown.options.Add(new Dropdown.OptionData(b.ToString()));
        }
        for (int c = 1; c < WorkerManager.WorkersList(Worker.Worker_Type.Sound).Count; c++) {
            _soundsDropDown.options.Add(new Dropdown.OptionData(c.ToString()));
        }
    }

    public void CreateGame()
    {
        Game newGame = new Game(_gameName,_gameGraphics,_gameOptimization,_gameSounds,_gamePrefab);
        GameManager.GetInstance().CurrentGames.Add(newGame);
    }


    public void OnValueChanged()
    {
        _gameGraphics = (int) _graphicsSlider.value;
        _graphicsText.text = string.Format("{0}/10", _gameGraphics);
        _gameOptimization = (int) _optimizationSlider.value;
        _optimizationText.text = string.Format("{0}/10", _gameOptimization);
        _gameSounds = (int) _soundsSlider.value;
        _soundText.text = string.Format("{0}/10", _gameSounds);


    }

    public void OnEndEdit()
    {
        _gameName = _gameNameField.text;


    }




}